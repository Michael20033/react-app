import React from 'react';
import s from './Card.module.scss';

class Card extends React.Component {

    state = {
        done: false,
    }

    onCardClick = () => {
        this.setState({
            done: true,
        })
    }

    render() {
        const { pizza, price} = this.props;
        const { done } = this.state;
        const cardClass = [s.card];
        if (done) {
            cardClass.push(s.done);
        } 

        return (
            <table className={s.card}> 
                <div 
            className={cardClass.join(' ')}
            onClick={this.onCardClick}
            >
                <div className={s.cardInner}>
                    <div className={s.cardFront}>
                        { pizza }
                    </div>
                    <div className={s.cardBack}>
                        { price }
                    </div>
                </div>  
            </div>
            </table>
        );
    }
}
export default Card;