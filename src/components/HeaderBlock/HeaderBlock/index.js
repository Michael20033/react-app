import React from 'react';
import  k from './HeaderBlock.module.scss';


const HeaderBlock = ({ hideBackground = false,  children}) => {
    const styleCover = hideBackground ? {backgroundImage: "none"} : {};
    return (
        <div className={k.cover} style={styleCover} >
            <div className={k.wrap}>
                {children}
            </div>
        </div>
    );
}

export default HeaderBlock;
