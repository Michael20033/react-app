import React from 'react';
import d from './FooterBlock.module.scss';



const FooterBlock = ({insta,telegram}) => {
    return (<div className={d.footer}>
        <div  >
        <h1 >{insta}</h1>
        <h1>{telegram}</h1>
        <h1>With love AgniGupta</h1>
        </div>
    </div>
    );
}

export default FooterBlock;