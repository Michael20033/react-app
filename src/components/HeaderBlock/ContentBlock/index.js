import React from 'react';
import s from './ContentBlock.module.scss';



const ContentBlock = ({ children}) => {
    return (<div>
        <div className={s.cover}>{children}
        </div>
    </div>
       
    );
}

export default ContentBlock;