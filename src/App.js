import React from 'react';
import ContentBlock from './components/HeaderBlock/ContentBlock';
import HeaderBlock from './components/HeaderBlock/HeaderBlock';
import FooterBlock from './components/HeaderBlock/FooterBlock';
import Paragraph from './components/HeaderBlock/Paragraph';
import Header from './components/HeaderBlock/Header';
import Card from './components/HeaderBlock/Card';
import Image from './components/HeaderBlock/Image';

const wordsList = [
  {
    pizza: 'Пица паперони с томатами',
      price: '87.99 грн'
  },
  {
    pizza: 'Пица ',
      price: '87.99 грн'
  },
  {
    pizza: 'Пица с колбасками',
      price: '87.99 грн'
  },
  {
    pizza: 'Гавайская',
      price: '119.99 грн'
  },
  {
    pizza: 'Паперони',
      price: '119.99 грн'
  },
  {
    pizza: 'Нью Йорк',
      price: '119.99 грн'
  },
  {
    pizza: 'Регина',
      price: '119.99 грн'
  },
  {
      pizza: 'Прошуто',
      price: '119.99 грн'
  },
  {
    pizza: '4 Сыры',
      price: '147.99 грн'
  },
  ];

const App = () => {
  return (
    <>
      <HeaderBlock >
        <Header>Dominno's Pizza</Header>
        <Paragraph>Знижка -20 на все в піцерії </Paragraph>
      </HeaderBlock>

      <HeaderBlock hideBackground >
        <Image />
        <Paragraph>Соус рікота, потрійні томати, моцарелла, фета, соус песто, рукола 640г</Paragraph>
        
        <Paragraph>185 грн</Paragraph>
      </HeaderBlock>
      <ContentBlock><div>
        {
          wordsList.map(({ pizza, price }, index ) => <Card key={index} pizza={pizza} price={price} />)
        }
      </div></ContentBlock>

      
      <ContentBlock>
        <Header>Улюблюна піццерія та суші в Xмельницькому, велике меню, можливіть замовити до дому, постійні знижки, якісне обслуговування. Завжди вам раді!.</Header>
      </ContentBlock>

      <FooterBlock
        insta="lalalala"
        telegram="@mihuiill"
      />

    </>
  );
}

export default App;
